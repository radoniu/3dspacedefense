﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipOrbit : MonoBehaviour {
    private Transform tf;
    public float speed;

	// Use this for initialization
	void Start () {
        tf = GetComponent<Transform>();
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.DownArrow))
        {
            tf.Rotate(-speed, 0, 0);
        }

        if (Input.GetKey(KeyCode.UpArrow))
        {
            tf.Rotate(speed, 0, 0);
        }

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            tf.Rotate(0, -speed, 0);
        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            tf.Rotate(0, speed, 0);
        }
    }

}
