﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotation : MonoBehaviour {
    public float turnSpeed;
    private Transform tf;

    // Use this for initialization
    void Start () {
        tf = GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update () {
        tf.Rotate(0, turnSpeed, 0);
	}
}
